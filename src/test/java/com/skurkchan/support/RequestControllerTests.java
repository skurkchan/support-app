package com.skurkchan.support;

import com.skurkchan.support.model.Request;
import com.skurkchan.support.model.RequestStatus;
import com.skurkchan.support.service.RequestService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;

import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SupportApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations = "classpath:application-test.properties")
public class RequestControllerTests {

    private MockMvc mockMvc;
    @Autowired
    RequestService requestService;
    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();

        List<Request> requests = Arrays.asList(
                new Request(1L, RequestStatus.OPEN, "need some support 1", ""),
                new Request(2L, RequestStatus.OPEN, "need some support 2", ""));

        requests.forEach(requestService::addRequest);
    }


    @Test
    public void admin_get_all() throws Exception {

        mockMvc.perform(get("/admin/requests")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].message", is("need some support 1")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].message", is("need some support 2"))).andReturn();
    }

    @Test
    public void admin_get_all_open() throws Exception {

        Request testRequest = new Request();
        testRequest.setId(1L);
        testRequest.setResponse("response");

        requestService.answerAndClose(testRequest);

        mockMvc.perform(get("/admin/requests/open")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(2)))
                .andExpect(jsonPath("$[0].status", is("OPEN")))
                .andExpect(jsonPath("$[0].message", is("need some support 2"))).andReturn();
    }

    @Test
    public void user_add_request() throws Exception {

        mockMvc.perform(
                post("/user/requests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"message\" : \"need some support 3\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void user_get_status() throws Exception {

        mockMvc.perform(get("/user/requests/status/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("status", is("OPEN")))
                .andExpect(jsonPath("response", isEmptyOrNullString()));

        mockMvc.perform(
                put("/admin/requests/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"response\" : \"answered\"}"))
                .andExpect(status().isOk());

        mockMvc.perform(get("/user/requests/status/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("status", is("CLOSED")))
                .andExpect(jsonPath("response", not(isEmptyOrNullString())));
    }
}
