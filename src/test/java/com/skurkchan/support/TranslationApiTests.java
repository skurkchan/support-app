package com.skurkchan.support;

import com.skurkchan.support.model.Request;
import com.skurkchan.support.translation.TranslationManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class TranslationApiTests {

    @Autowired
    private TranslationManager translationManager;

    @Test
    public void translate_to_native_lang_test(){
        Request foreignRequest = new Request();
        foreignRequest.setMessage("Ich brauche Hilfe, weil es ein Problem gibt!");
        foreignRequest.setResponse("Please wait. Your problem is being solved. Thank you!");

        String translatedResponse = translationManager.translateToNative(foreignRequest);

        assertEquals(translationManager.getMessageLang(translatedResponse), "de");
    }

    @Test
    public void translate_to_english_test(){

        String russian = "Нужна помощь";
        String french = "Besoin d'aide";
        String spanish = "Necesita ayuda";
        String expectedTranslation = "Need help";

        String toEnglishFromRussian = translationManager.translateToEnglish(russian);
        String toEnglishFromFrench = translationManager.translateToEnglish(french);
        String toEnglishFromSpanish = translationManager.translateToEnglish(spanish);

        assertEquals(toEnglishFromRussian, expectedTranslation);
        assertEquals(toEnglishFromFrench, expectedTranslation);
        assertEquals(toEnglishFromSpanish, expectedTranslation);
    }

}
