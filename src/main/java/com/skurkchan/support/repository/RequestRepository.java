package com.skurkchan.support.repository;

import com.skurkchan.support.model.Request;
import com.skurkchan.support.model.RequestStatus;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request, Long>{

    Iterable<Request> findAllByStatus(RequestStatus status);
}
