package com.skurkchan.support.service;

import com.skurkchan.support.model.Request;
import com.skurkchan.support.model.RequestStatus;
import com.skurkchan.support.model.RequestUserInfo;
import com.skurkchan.support.repository.RequestRepository;
import com.skurkchan.support.translation.TranslationManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestService {

    @Autowired
    private RequestRepository repository;
    @Autowired
    private TranslationManager translationManager;


    public List<Request> getAllRequests(){
        List<Request> allRequests = new ArrayList<>();
        repository.findAll().forEach(allRequests::add);
        allRequests.parallelStream()
                .forEach(r -> r.setMessage(translationManager.translateToEnglish(r.getMessage())));
        return allRequests;

    }

    public List<Request> getAllOpenRequests(){
        List<Request> openRequests = new ArrayList<>();
        repository.findAllByStatus(RequestStatus.OPEN).forEach(openRequests::add);
        openRequests.parallelStream()
                .forEach(r -> r.setMessage(translationManager.translateToEnglish(r.getMessage())));
        return openRequests;
    }

    public Request getRequest(Long id){
        Request request = repository.findOne(id);
        request.setMessage(translationManager.translateToEnglish(request.getMessage()));
        return request;
    }

    public void answerAndClose(Request request){
        request.setStatus(RequestStatus.CLOSED);
        repository.save(request);
    }

    public RequestUserInfo getRequestUserInfo(Long id){
        RequestUserInfo userInfo = new RequestUserInfo();
        Request request = repository.findOne(id);

        if (request.getStatus() == RequestStatus.CLOSED) {
            userInfo.setStatus(RequestStatus.CLOSED);
            userInfo.setResponse(translationManager.translateToNative(request));
        }

        return userInfo;
    }

    public void addRequest(Request request){
        request.setStatus(RequestStatus.OPEN);
        request.setResponse("");
        repository.save(request);
    }
}
