package com.skurkchan.support.controllers;

import com.skurkchan.support.model.Request;
import com.skurkchan.support.model.RequestUserInfo;
import com.skurkchan.support.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public final class RequestController {

    @Autowired
    private RequestService service;

    @RequestMapping(method = RequestMethod.GET, value = "/admin/requests")
    public List<Request> getAllRequests(){
        return service.getAllRequests();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/requests/open")
    public List<Request> getAllOpenRequests(){
        return service.getAllOpenRequests();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/requests/{id}")
    public Request getRequest(@PathVariable Long id){
        return service.getRequest(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/admin/requests/{id}")
    public void answerAndClose(@RequestBody Request request, @PathVariable Long id){
        request.setId(id);
        service.answerAndClose(request);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/user/requests/status/{id}")
    public RequestUserInfo getRequestUserInfo(@PathVariable Long id){
        return service.getRequestUserInfo(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/requests")
    public void addRequest(@RequestBody Request request){
        service.addRequest(request);
    }

}
