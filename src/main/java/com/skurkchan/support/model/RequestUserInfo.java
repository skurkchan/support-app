package com.skurkchan.support.model;

public final class RequestUserInfo {

    private RequestStatus status = RequestStatus.OPEN;
    private String response = "";

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
