package com.skurkchan.support.model;


public enum RequestStatus {
    OPEN("OPEN"),
    CLOSED("CLOSED");

    private String statusName;

    RequestStatus(String value) {
        this.statusName = value;
    }

    @Override
    public String toString() {
        return statusName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
