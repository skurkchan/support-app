package com.skurkchan.support.translation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skurkchan.support.model.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@Component
public final class TranslationManager {

    @Value("${yandex.translation.key}")
    private String YANDEX_TRANSLATION_KEY;
    @Value("${yandex.translation.url}")
    private String YANDEX_TRANSLATION_URL;

    public String translateToEnglish(final String originalMessage){
        String url = YANDEX_TRANSLATION_URL + "translate?lang=en&key=" + YANDEX_TRANSLATION_KEY;
        String jsonApiResponse = getResponseFromApi(url, originalMessage);
        return getTextFromResponse(jsonApiResponse);
    }

    public String translateToNative(final Request request){
        String lang = getMessageLang(request.getMessage());
        String url = YANDEX_TRANSLATION_URL + "translate?lang=" + lang + "&key=" + YANDEX_TRANSLATION_KEY;
        String jsonApiResponse = getResponseFromApi(url, request.getResponse());
        return getTextFromResponse(jsonApiResponse);
    }

    public String getMessageLang(final String message) {
        String url = YANDEX_TRANSLATION_URL + "detect?key=" + YANDEX_TRANSLATION_KEY;
        String jsonApiResponse = getResponseFromApi(url, message);
        return getLangFromResponse(jsonApiResponse);
    }

    private String getResponseFromApi(final String url, final String message){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("text", message);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(url, request , String.class);

        return response.getBody();
    }

    private String getTextFromResponse(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonString);
            return jsonNode.get("text").get(0).textValue();
        } catch (Exception e) {
            RuntimeException ex = new RuntimeException("Error getting translated text", e);
            ex.printStackTrace();
            throw ex;
        }
    }

    private String getLangFromResponse(String jsonString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(jsonString);
            return jsonNode.get("lang").textValue();
        } catch (Exception e) {
            RuntimeException ex = new RuntimeException("Error detecting message language", e);
            ex.printStackTrace();
            throw ex;
        }
    }
}
