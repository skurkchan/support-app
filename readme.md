**URIs:**

GET `/admin/requests` - список всех запросов

GET `/admin/requests/open` - список всех открытых запросов

GET `/admin/requests/{id}` - получить запрос по id

PUT `/admin/requests/{id}` - обновить запрос (ответ на обращение)

GET `/user/requests/status/{id}` - статус запроса для пользователя

POST `/user/requests` - создание пользователем нового запроса

**Приложение использует security.**

URLs, начинающиеся с `admin` - для сотрудника поддержки, с `user` - соответственно, для пользователей

**login/password:**
1. admin/admin
2. user/user